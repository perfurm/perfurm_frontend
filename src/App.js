import { BrowserRouter, Route, Routes } from 'react-router-dom';
import DefaultLayout from './component/layout/DefaultLayout';
import { getData } from './Routes/PublicRoutes';
import { useState, useEffect } from 'react';

function App() {
	const [data, setData] = useState([]);

	useEffect(() => {
		getMyData();
		// eslint-disable-next-line
	}, []);

	const getMyData = async () => {
		try {
			const res = await getData();
			setData(res);
		} catch (err) {
			console.error(err);
		}
	};

	return (
		<BrowserRouter>
			<div className="App">
				<Routes>
					{data.map((route, index) => {
						const Page = route.element;
						return (
							<Route
								key={index}
								path={route.path}
								element={<DefaultLayout>{Page}</DefaultLayout>}
							/>
						);
					})}
				</Routes>
			</div>
		</BrowserRouter>
	);
}

export default App;
