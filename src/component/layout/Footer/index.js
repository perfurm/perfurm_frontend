import styles from './Footer.module.scss';
import classNames from 'classnames/bind';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
	faInstagram,
	faFacebook,
	faYoutube,
} from '@fortawesome/free-brands-svg-icons';
import { faLocationDot } from '@fortawesome/free-solid-svg-icons';
import { Link } from 'react-router-dom';

const cx = classNames.bind(styles);

function Footer() {
	return (
		<div className={cx('footer-wrapper')}>
			<div className={cx('footer-icon')}>
				<div className={cx('footer-icon__item')}>
					<a href="/">
						<FontAwesomeIcon icon={faInstagram} />
					</a>
					<a href="/">
						<FontAwesomeIcon icon={faFacebook} />
					</a>
					<a href="/">
						<FontAwesomeIcon icon={faYoutube} />
					</a>
					<a href="/">
						<FontAwesomeIcon icon={faLocationDot} />
					</a>
				</div>
			</div>
			<div className={cx('footer-content')}>
				<div className={cx('footer-content-loi')}>
					<div className={cx('footer-content__wrapper')}>
						<div className={cx('footer-content__item')}>
							<div className={cx('footer-conten-align')}>
								<div className={cx('footer-content__title')}>parfurm store</div>
								<div className={cx('footer-content__body')}>
									<Link to={''}>ưu đãi thành viên</Link>
									<Link to={''}>tài khoản</Link>
									<Link to={''}>tuyển dụng</Link>
								</div>
							</div>
						</div>
						<div className={cx('footer-content__itemlargest')}>
							<div className={cx('footer-conten-align')}>
								<div className={cx('footer-content__title')}>
									chính sách bán hàng
								</div>
								<div className={cx('footer-content__body')}>
									<Link to={''}>phương thức vận chuyển</Link>
									<Link to={''}>câu hỏi thường gặp</Link>
									<Link to={''}>điều khoản và điện kiện sử dụng</Link>
									<Link to={''}>điều khoản và điều kiện bán hàng</Link>
									<Link to={''}>thông báo pháp lýp</Link>
								</div>
							</div>
						</div>
						<div className={cx('footer-content__item')}>
							<div className={cx('footer-conten-align')}>
								<div className={cx('footer-content__title')}>
									thông tin chung
								</div>
								<div className={cx('footer-content__body')}>
									<Link to={''}>giới thiệu</Link>
									<Link to={''}>blog</Link>
									<Link to={''}>liên hệ</Link>
									<Link to={''}>sản phẩm</Link>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div className={cx('footer-end')}>
				<p className={cx('footer-end__body')}>
					© parfurm 2023 | all right reserved
				</p>
			</div>
		</div>
	);
}

export default Footer;
