import Tippy from '@tippyjs/react/headless';
import styles from './MenuBar.module.scss';
import classNames from 'classnames/bind';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleRight } from '@fortawesome/free-solid-svg-icons';
import { Link } from 'react-router-dom';
import { useState } from 'react';

const cx = classNames.bind(styles);

function MenuLi({ text = [] }) {
	const [didnex, setDidnex] = useState(null);
	const menuItem = text.map((data, index) => {
		if (data.list === null) {
			return (
				<li className={cx('wrapper-li')} key={index}>
					<div className={cx('div-li')}>
						<div className={cx('link-li')}>
							<Link to={'/Nuoc-hoa-nam'}>{data.title}</Link>
						</div>
					</div>
				</li>
			);
		} else {
			return (
				<Tippy
					key={index}
					interactive
					visible={didnex === index}
					placement="right"
					render={(attrs) => (
						<div
							className={cx('wrapper-popper__menu')}
							onMouseEnter={() => {
								setDidnex(index);
							}}
							onMouseLeave={() => {
								setDidnex(null);
							}}
							tabIndex="-1"
							{...attrs}
						>
							<ul className={cx('formen')}>
								<li>{data.list.list1}</li>
								<li>{data.list.list2}</li>
							</ul>
						</div>
					)}
				>
					<li className={cx('wrapper-li')}>
						<div
							className={cx('div-li')}
							onMouseEnter={() => {
								setDidnex(index);
							}}
							onMouseLeave={() => {
								setDidnex(null);
							}}
						>
							<div className={cx('link-li')}>
								<Link to={'/Nuoc-hoa-nam'}>{data.title}</Link>
							</div>
							<FontAwesomeIcon icon={faAngleRight} />
						</div>
					</li>
				</Tippy>
			);
		}
	});

	return <ul className={cx('wrapper-ul')}>{menuItem}</ul>;
}

export default MenuLi;
