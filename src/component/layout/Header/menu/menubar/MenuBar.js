import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars, faClose } from '@fortawesome/free-solid-svg-icons';
import styles from './MenuBar.module.scss';
import classNames from 'classnames/bind';
import { useState } from 'react';
import { faUser } from '@fortawesome/free-regular-svg-icons';
import MenuLi from './menuLi';

const cx = classNames.bind(styles);

function MenuBar() {
	const [menuClick, setMenuClick] = useState(false);

	const PRODUCT = [
		{ title: 'nước hoa nam', list: null },
		{ title: 'nước hoa nữ', list: null },
		{ title: 'Thương Hiệu', list: null },
	];

	return (
		<div className={cx('wrapper')}>
			<button
				className={cx('btn-menu')}
				onClick={() => {
					setMenuClick(!menuClick);
				}}
			>
				<FontAwesomeIcon icon={faBars} />
			</button>
			<div
				className={menuClick ? cx('menu-fullbox-visible') : cx('menu-fullbox')}
			>
				<div className={menuClick ? cx('menu-box-visible') : cx('menu-box')}>
					<div className={cx('header-menu')}>
						<div className={cx('Signup-menu')}>
							<FontAwesomeIcon icon={faUser} className={cx('menu-user')} />
							<a className={cx('link-signup')} href={'/account'}>
								Đăng kí
							</a>
						</div>
						<button
							className={cx('close-menu')}
							onClick={() => {
								setMenuClick(false);
							}}
						>
							<FontAwesomeIcon icon={faClose} />
						</button>
					</div>
					<div className={cx('content-menu')}>
						<MenuLi text={PRODUCT} />
					</div>
				</div>
				<div
					className={cx('overlay__actived')}
					onClick={() => {
						setMenuClick(false);
					}}
				></div>
			</div>
		</div>
	);
}

export default MenuBar;
