import styles from './SearchResult.module.scss';
import classNames from 'classnames/bind';

const cx = classNames.bind(styles);

function SearchResult(data = []) {
	console.log(data);
	return (
		<div className={cx('searchresult')}>
			<div className={cx('wrapper')}>
				<div className={cx('title')}>
					<h3>Kết quả tìm kiếm</h3>
				</div>
				<div className={cx('content')}>
					{data.data.map((data, index) => {
						return (
							<div className={cx('item')} key={index}>
								<div className={cx('image')}>
									<img
										className={cx('image-product')}
										alt={'nuoc hoa'}
										src={data.image}
									/>
								</div>
								<div className={cx('item-title')}>
									<h3 className={cx('product-title')}>{data.brandName}</h3>
									<span className={cx('price')}>{data.price.ml60}</span>
								</div>
							</div>
						);
					})}
				</div>
			</div>
		</div>
	);
}

export default SearchResult;
