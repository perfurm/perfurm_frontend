import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import styles from './Search.module.scss';
import classNames from 'classnames/bind';
import { useEffect, useRef, useState } from 'react';
import Tippy from '@tippyjs/react/headless';
import SearchResult from './SearchResult';
import useDebounce from './useDebounce';

const cx = classNames.bind(styles);

function Search() {
	const [hovered, setHovered] = useState(false);
	const [focused, setFocus] = useState(false);
	const [valued, setValued] = useState('');
	const [DB, setDB] = useState([]);

	const Ref = useRef();

	const debounce = useDebounce(valued, 500);

	useEffect(() => {
		if (!debounce.trim()) {
			setDB([]);
			return;
		}
		fetch(
			`https://perfurm-backend.onrender.com/product/searchproductdb?brandName=${encodeURIComponent(
				debounce.trim()
			)}&limit=4`,
			{}
		)
			.then((res) => res.json())
			.then((data) => setDB(data));
	}, [debounce]);

	return (
		<Tippy
			interactive
			placement="bottom-end"
			visible={DB.length > 0}
			render={(attrs) => (
				<div className={cx('cart-shooping')} tabIndex="-1" {...attrs}>
					<SearchResult data={DB} />
				</div>
			)}
		>
			<div
				className={cx('wrapper-search')}
				onMouseLeave={() => setHovered(false)}
				onMouseEnter={() => setHovered(true)}
			>
				<button
					className={cx('btn-search')}
					onClick={() => Ref.current.focus()}
				>
					<FontAwesomeIcon
						className={focused ? cx('search-icon') : ''}
						icon={faSearch}
					/>
				</button>

				<div
					className={
						hovered || focused
							? cx('wrapper-search-input__open')
							: cx('wrapper-search-input__close')
					}
				>
					<div className={cx('search-input__div')}>
						<input
							ref={Ref}
							className={cx('search-input')}
							type={'text'}
							placeholder={'Tim kiem san pham'}
							onFocus={() => {
								setFocus(true);
							}}
							onBlur={() => {
								setFocus(false);
								setValued('');
							}}
							onChange={(e) => {
								setValued(e.target.value);
							}}
							value={valued}
						/>
					</div>
				</div>
				<span
					className={
						focused || hovered ? cx('search-line__open') : cx('search-line')
					}
				></span>
			</div>
		</Tippy>
	);
}

export default Search;
