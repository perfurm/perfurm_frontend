import styles from './header.module.scss';
import classNames from 'classnames/bind';
import { Link } from 'react-router-dom';
import Logo from '../../Logo/logoheader';
import MenuBar from './menu/menubar/MenuBar';
import Search from './menu/search/Search';
import React from 'react';
import CartShopping from './action/cart/Cart';

const cx = classNames.bind(styles);

function Header() {
	return (
		<div className={cx('header')}>
			<div className={cx('menu')}>
				<MenuBar />
				<Search />
			</div>
			<div className={cx('logo')}>
				<Link to="/" className={cx('a-logo')}>
					<span>
						<Logo className={cx('svg-logo')} />
					</span>
				</Link>
			</div>
			<div className={cx('action')}>
				<CartShopping />
			</div>
		</div>
	);
}

export default Header;
