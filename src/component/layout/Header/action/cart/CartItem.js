import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from './Cart.module.scss';
import classNames from 'classnames/bind';
import { Link } from 'react-router-dom';
import { Store, action } from '../../../../../store';

const cx = classNames.bind(styles);

function NoProduct() {
	return (
		<div className={cx('noproduct-wrapper')}>
			<div className={cx('image-noproduct')}>
				<img
					src={require('../image/empty.png')}
					alt={'no-product'}
					className={cx('image')}
				/>
			</div>
			<span className={cx('noproduct-title')}>giỏ hàng chưa có sản phẩm</span>
		</div>
	);
}

function CartItem() {
	const [state, dispatch] = Store();

	const Item = state.listCart.map((data, index) => {
		return (
			<li className={cx('li-item')} key={index}>
				<div className={cx('item-wrapper')}>
					<Link
						to={`/product${data.path}`}
						className={cx('product-item__image')}
					>
						<img src={data.image} className={cx('image-item')} alt={'hello'} />
					</Link>

					<div className={cx('product-item')}>
						<Link
							to={`/product${data.path}`}
							className={cx('product-item__name')}
						>
							{data.brandName}
						</Link>
						<span className={cx('so-luong')}>So luong: {data.soluong}</span>
					</div>
					<div className={cx('trash-item')}>
						<button
							className={cx('trash-button')}
							onClick={() => {
								dispatch(action.deleteProduct(index));
							}}
						>
							<FontAwesomeIcon icon={faTrash} />
						</button>
					</div>
				</div>
			</li>
		);
	});
	return (
		<div className={cx('cart-item__wrapper')}>
			{state.listCart.length > 0 ? (
				<ul className={cx('cart-ul')}>
					{Item}
					<div className={cx('cart-item__bottom')}>
						<button
							className={cx('cart-btn')}
							onClick={() => {
								window.location.href = '/order';
							}}
						>
							Thanh toán
						</button>
					</div>
				</ul>
			) : (
				<NoProduct />
			)}
		</div>
	);
}

export default CartItem;
