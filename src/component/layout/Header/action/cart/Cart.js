import { faCartShopping } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from './Cart.module.scss';
import classNames from 'classnames/bind';
import Tippy from '@tippyjs/react/headless';
import CartItem from './CartItem';
import { useEffect, useState } from 'react';
import { Store } from '../../../../../store';

const cx = classNames.bind(styles);

function CartShopping() {
	const [cartFocus, setCartFocus] = useState(false);
	const [state] = Store();
	useEffect(() => {
		setCartFocus(true);
	}, [state.listCart]);

	return (
		<Tippy
			interactive
			placement="bottom"
			trigger="click"
			render={(attrs) => (
				<div className={cx('cart-shooping')} tabIndex="-1" {...attrs}>
					<CartItem />
				</div>
			)}
		>
			<button
				className={cartFocus ? cx('cart-shoping__active') : cx('cart-shoping')}
				onClick={() => {
					setCartFocus(false);
				}}
			>
				<FontAwesomeIcon icon={faCartShopping} />
				<div
					className={
						cartFocus && state.listCart.length > 0
							? cx('cart-quantity')
							: cx('cart-quantity-hidden')
					}
				>
					<span className={cx('cart-number')}>{state.listCart.length}</span>
				</div>
			</button>
		</Tippy>
	);
}

export default CartShopping;
