import styles from './DefaultLayout.module.scss';
import classNames from 'classnames/bind';
import Header from './Header';
import Content from './Conten';
import Footer from './Footer';

const cx = classNames.bind(styles);

function DefaultLayout({ children }) {
	return (
		<div className={cx('container')}>
			<Header />
			<Content children={children} />
			<Footer />
		</div>
	);
}

export default DefaultLayout;
