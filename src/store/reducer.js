import {
	ADD_PRODUCT,
	DELETE_PRODUCT,
	SET_PRODUCT,
	PLUS_QUANTITY,
	MINUS_QUANTITY,
	ADD_DB,
} from './constant';

const initState = {
	DB: [],
	listCart: [],
};

function reducer(state, action) {
	let newState = {};
	switch (action.type) {
		case ADD_PRODUCT:
			if (state.listCart.length > 0) {
				state.listCart.map((item, index) => {
					if (item.brandName === action.load.brandName) {
						state.listCart[index].soluong += action.load.soluong;
						console.log(action.load.soluong);
						return (newState = {
							...state,
							listCart: [...state.listCart],
						});
					} else {
						return (newState = {
							...state,
							listCart: [...state.listCart, action.load],
						});
					}
				});
			} else {
				return (newState = {
					...state,
					listCart: [...state.listCart, action.load],
				});
			}

			break;
		case DELETE_PRODUCT:
			const newList = [...state.listCart];
			newList.splice(action.Payload, 1);
			newState = {
				...state,
				listCart: newList,
			};
			break;

		case SET_PRODUCT:
			newState = {
				...state,
				listCart: action.payload,
			};
			break;
		case PLUS_QUANTITY:
			if (state.listCart.length > 0) {
				state.listCart.map((item, index) => {
					if (index === action.payload) {
						item.soluong += 1;
					}

					return (newState = {
						...state,
						listCart: [...state.listCart],
					});
				});
			}

			break;
		case MINUS_QUANTITY:
			if (state.listCart.length > 0) {
				state.listCart.map((item, index) => {
					if (index === action.payload) {
						item.soluong -= 1;
					}

					return (newState = {
						...state,
						listCart: [...state.listCart],
					});
				});
			}

			break;

		case ADD_DB:
			newState = {
				...state,
				DB: action.payload,
			};
			break;
		default:
			throw new Error('loi roi');
	}

	return newState;
}

export { initState };
export default reducer;
