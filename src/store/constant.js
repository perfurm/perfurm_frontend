export const ADD_PRODUCT = 'add_product';
export const DELETE_PRODUCT = 'delete_product';
export const SET_PRODUCT = 'set_product';
export const PLUS_QUANTITY = 'plus_quantity';
export const MINUS_QUANTITY = 'minus_quantity';
export const ADD_DB = 'add_db';
