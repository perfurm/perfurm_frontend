import {
	ADD_PRODUCT,
	DELETE_PRODUCT,
	SET_PRODUCT,
	PLUS_QUANTITY,
	MINUS_QUANTITY,
	ADD_DB,
} from './constant';

export const addProduct = (Payload) => ({
	type: ADD_PRODUCT,
	load: Payload,
});

export const deleteProduct = (Payload) => ({
	type: DELETE_PRODUCT,
	Payload,
});

export const setProduct = (payload) => ({
	type: SET_PRODUCT,
	payload,
});

export const plusQuantity = (payload) => ({
	type: PLUS_QUANTITY,
	payload,
});

export const minusQuantity = (payload) => ({
	type: MINUS_QUANTITY,
	payload,
});

export const add_Db = (payload) => ({
	type: ADD_DB,
	payload,
});
