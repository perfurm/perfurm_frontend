import { useReducer } from 'react';
import Context from './contexts';
import reducer, { initState } from './reducer';
import { useEffect } from 'react';
import { useLocalStorage } from './hook';

function Provider({ children }) {
	const [getproduct, setProduct] = useLocalStorage('product');
	const [state, dispatch] = useReducer(reducer, getproduct() || initState);

	useEffect(() => {
		setProduct(state);
	}, [setProduct, state]);
	return (
		<Context.Provider value={[state, dispatch]}>{children}</Context.Provider>
	);
}

export default Provider;
