import Home from '../Page/home/home';
import Order from '../Page/order/index';
import Product from '../Page/product/Product';
import Account from '../Page/account/Account.';

export async function getData() {
	const category = await fetch(
		'https://perfurm-backend.onrender.com/product/productdb'
	);
	const DB = await category.json();
	let NormalPage = [
		{ path: '/', element: <Home /> },
		{ path: '/order', element: <Order /> },
		{ path: '/account', element: <Account /> },
	];

	let productPage = DB.map((data, index) => {
		var Obj = {
			path: '/product' + data.path,
			element: <Product data={data} />,
			id: index,
		};
		return Obj;
	});
	const PublicRoutes = NormalPage.concat(productPage);
	return PublicRoutes;
}
