import styles from './Oder.module.scss';
import classNames from 'classnames/bind';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faXmark, faPlus, faMinus } from '@fortawesome/free-solid-svg-icons';
import { Store } from '../../store';
import { deleteProduct, minusQuantity, plusQuantity } from '../../store/action';
import { useEffect, useRef, useState } from 'react';
import UIkit from 'uikit';

const cx = classNames.bind(styles);

function Order() {
	const [state, dispatch] = Store();

	const data = state.listCart;
	const [price, setPrice] = useState('0');
	const [totalPrice, setTotalPrice] = useState(price);
	const [voucher, setVoucher] = useState(0);
	useEffect(() => {
		setPrice('0');
		let Price = 0;

		data.map((Item) => {
			let itemPice = parseInt(Item.priceSelected.split('.').join(''));
			let itemSoluong = parseInt(Item.soluong);
			Price = Price + itemPice * itemSoluong;
			return Price;
		});
		setPrice(() => {
			let PriceString = Price.toString();
			let i = PriceString.length - 3;
			while (i > 0) {
				PriceString = PriceString.slice(0, i) + '.' + PriceString.slice(i);
				i = i - 3;
			}
			return PriceString;
		});
	}, [data]);
	useEffect(() => {
		setTotalPrice(() => {
			const intPrice = parseInt(price.split('.').join(''));
			const intVoucher = parseInt(voucher);
			const totalPrice = intPrice - intPrice * intVoucher * 0.01;
			let PriceString = totalPrice.toString();
			let i = PriceString.length - 3;
			while (i > 0) {
				PriceString = PriceString.slice(0, i) + '.' + PriceString.slice(i);
				i = i - 3;
			}
			return PriceString;
		});
	}, [price, voucher]);

	const modalRef = useRef();

	useEffect(() => {
		UIkit.modal(modalRef.current, {
			'cls-panel': 'uk-modal-dialog',
		});
	}, []);

	const [maVoucher, setMaVoucher] = useState('');
	function HandleConfirmVoucher() {
		console.log(maVoucher);
		fetch(
			`https://perfurm-backend.onrender.com/voucher?maVoucher=${encodeURIComponent(
				maVoucher
			)}`,
			{}
		)
			.then((res) => res.json())
			.then((res) => {
				if (res.length < 1) {
					alert('Voucher khong ton tai');
					return;
				}
				console.log(res[0].phanTramGiamGia);
				setVoucher(res[0].phanTramGiamGia);
			});
	}
	return (
		<div className={cx('order-wrapper')}>
			<div className={cx('order-container')}>
				<div className={cx('col-7')}>
					<div className={cx('row')}>
						<h1 className={cx('order-title')}>Sản phẩm đã chọn</h1>
					</div>
					<div className={cx('row')}>
						<div className={cx('order-product')}>
							{data.map((data, index) => {
								return (
									<div className={cx('order-product__item')} key={index}>
										<div className={cx('order-product__item__image')}>
											<img
												src={data.image}
												alt={'heello'}
												className={cx('image-product')}
											/>
										</div>
										<div className={cx('order-product__item__name')}>
											<h3 className={cx('product-name')}>{data.brandName}</h3>
											<span>{data.priceSelected} đ</span>
										</div>
										<div className={cx('order-product__quantity')}>
											<div className={cx('quantity')}>
												<span
													className={cx('quantity-plus')}
													onClick={() => {
														dispatch(plusQuantity(index));
													}}
												>
													<FontAwesomeIcon icon={faPlus} />
												</span>
												<input
													className={cx('product-quantity')}
													value={data.soluong}
												/>
												<span
													className={cx('quantity-minus')}
													onClick={() => {
														dispatch(minusQuantity(index));
													}}
												>
													<FontAwesomeIcon icon={faMinus} />
												</span>
											</div>
											<button
												className={cx('delete-product')}
												onClick={() => {
													dispatch(deleteProduct(index));
												}}
											>
												<FontAwesomeIcon icon={faXmark} />
											</button>
										</div>
									</div>
								);
							})}
							<div className={cx('order-product__voucher')}>
								<div className={cx('order-voucher')}>mã giảm giá</div>
								<div className={cx('order-voucher__value')}>
									<input
										type="text"
										placeholder="Nhập mã giảm giá của bạn"
										value={maVoucher}
										onChange={(e) => setMaVoucher(e.target.value)}
									/>
									<button
										className={cx('btn-voucher')}
										onClick={() => {
											HandleConfirmVoucher();
										}}
									>
										Xác nhận mã
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className={cx('col-3')}>
					<h4 className={cx('order-name')}>Phiếu thanh toán</h4>
					<div className={cx('order-price')}>
						<span>Tạm tính</span>
						<span className={cx('order-price__value')}>{price} đ</span>
					</div>
					<hr></hr>
					<div className={cx('order-price')}>
						<span>Tổng</span>
						<span className={cx('order-price__value')}>{totalPrice} đ</span>
					</div>
					<div className={cx('order-process')}>
						<button
							className={cx('order-btn-process')}
							onClick={() => {
								UIkit.modal(modalRef.current).show();
							}}
						>
							TIẾN HÀNH THANH TOÁN
						</button>
					</div>
				</div>
			</div>

			<div ref={modalRef}>
				<div class="uk-modal-dialog uk-modal-body">
					<button
						class="uk-modal-close-default"
						type="button"
						data-uk-close
					></button>
					<h2 class="uk-modal-title">Thông tin thanh toán</h2>
					<div className={cx('inp-wrapper')}>
						<label htmlFor="">Họ và Tên:</label>
						<br />
						<input className={cx('inp')} placeholder={'họ và tên'} />
					</div>
					<div className={cx('inp-wrapper')}>
						<label htmlFor="">Số điện thoại: </label>
						<br />
						<input className={cx('inp')} placeholder={'Số điện thoại'} />
					</div>
					<div className={cx('inp-wrapper')}>
						<label htmlFor="">Email: </label>
						<br />
						<input className={cx('inp')} placeholder={'Email'} />
					</div>
					<div className={cx('inp-wrapper')}>
						<label htmlFor="">Địa chỉ: </label>
						<br />
						<input className={cx('inp')} placeholder={'Địa chỉ'} />
					</div>
					<div className={cx('btn-confirm')}>
						<button type="submit" onClick={() => {}} className={cx('btn')}>
							Xác nhận đơn
						</button>
					</div>
				</div>
			</div>
		</div>
	);
}

export default Order;
