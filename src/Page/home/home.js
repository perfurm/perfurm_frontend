import styles from './home.module.scss';
import classNames from 'classnames/bind';
import Carousel from 'better-react-carousel';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
	faAngleRight,
	faAngleLeft,
	faShield,
	faTruck,
	faGift,
} from '@fortawesome/free-solid-svg-icons';
import { Link } from 'react-router-dom';
import { useEffect, useRef, useState } from 'react';
import UIkit from 'uikit';

const cx = classNames.bind(styles);

function Home() {
	const arrowRight = () => {
		return (
			<span className={cx('arrow-right')}>
				<FontAwesomeIcon icon={faAngleRight} />
			</span>
		);
	};
	const arrowLeft = () => {
		return (
			<span className={cx('arrow-left')}>
				<FontAwesomeIcon icon={faAngleLeft} />
			</span>
		);
	};
	const MyDot = ({ isActive }) => (
		<span className={isActive ? cx('dot-active') : cx('dot')}></span>
	);
	const ref = useRef();
	useEffect(() => {
		UIkit.switcher(ref.current, {
			connect: '.uk-switcher',
			animation: 'uk-animation-fade',
		});
	}, []);

	const [DB, setDB] = useState([]);
	useEffect(() => {
		fetch('https://perfurm-backend.onrender.com/product/productdb', {})
			.then((res) => res.json())
			.then((data) => setDB(data));
	}, []);

	const DB_FOR_MEN = DB.filter((db) => db.gender === 'Nam');
	const DB_FOR_WOMEN = DB.filter((db) => db.gender === 'Nữ');
	const DB_FOR_UNISEX = DB.filter((db) => db.gender === 'Unisex');

	return (
		<div className={cx('home')}>
			<div className={cx('slideshow-container')}>
				<Carousel
					cols={1}
					rows={1}
					gap={9}
					loop
					showDots
					autoplay={3500}
					arrowRight={arrowRight}
					arrowLeft={arrowLeft}
					dot={MyDot}
				>
					<Carousel.Item>
						<img src={require('./image/chanel-hero-090323.webp')} alt={'anh'} />
					</Carousel.Item>
					<Carousel.Item>
						<img
							src={require('./image/Di-or-Sauv-ge-Parfu-m-100ml.png')}
							alt={'anh'}
						/>
					</Carousel.Item>
					<Carousel.Item>
						<img
							src={require('./image/2da738706d054ae08d923ea6154a28f7.jpeg')}
							alt={'anh'}
						/>
					</Carousel.Item>
					<Carousel.Item>
						<img
							src={require('./image/giorgio_armani_acqua_di_gio_absolu_75ml_8b3c8bf4e3d84f52b8e7abd39ad4771c_master.jpg')}
							alt={'anh'}
						/>
					</Carousel.Item>
				</Carousel>
			</div>
			{/* brand */}
			<div className={cx('brand-home')}>
				<div className={cx('brand-title')}>
					<h2 className={cx('brand-title__h2')}>Thương Hiệu Nổi Tiếng</h2>
				</div>
				<div className={cx('brand-wrapper')}>
					<div className={cx('brand-content')}>
						<div className={cx('brand-card')}>
							<Link to={'/le labo'} className={cx('link-brand')}>
								<img
									src={require('./image/nuoc-hoa-le-labo.png')}
									alt={'nuoc hoa lelabo'}
									className={cx('brand-img')}
								/>
							</Link>
							<Link to={'/le labo'} className={cx('link-brand')}>
								<img
									src={require('./image/nuoc-hoa-dior.png')}
									alt={'nuoc hoa lelabo'}
									className={cx('brand-img')}
								/>
							</Link>
						</div>
						<div className={cx('brand-card')}>
							<Link to={'/le labo'} className={cx('link-brand')}>
								<img
									src={require('./image/159133030_1044233219399119_4321418372070751780_n.png')}
									alt={'nuoc hoa lelabo'}
									className={cx('brand-img')}
								/>
							</Link>
							<Link to={'/le labo'} className={cx('link-brand')}>
								<img
									src={require('./image/2021-11-18.jpg')}
									alt={'nuoc hoa lelabo'}
									className={cx('brand-img')}
								/>
							</Link>
						</div>
						<div className={cx('brand-card')}>
							<Link to={'/le labo'} className={cx('link-brand')}>
								<img
									src={require('./image/nuoc-hoa-by-kilian.png')}
									alt={'nuoc hoa kilian'}
									className={cx('brand-img')}
								/>
							</Link>
							<Link to={'/le labo'} className={cx('link-brand')}>
								<img
									src={require('./image/nuoc-hoa-tomford.png')}
									alt={'nuoc hoa tomford'}
									className={cx('brand-img')}
								/>
							</Link>
						</div>
						<div className={cx('brand-card')}>
							<Link to={'/le labo'} className={cx('link-brand')}>
								<img
									src={require('./image/nuoc-hoa-ysl.png')}
									alt={'nuoc hoa lelabo'}
									className={cx('brand-img')}
								/>
							</Link>
							<Link to={'/le labo'} className={cx('link-brand')}>
								<img
									src={require('./image/Logo-500x500-px-2.webp')}
									alt={'nuoc hoa lelabo'}
									className={cx('brand-img')}
								/>
							</Link>
						</div>
						<div className={cx('brand-card', 'd-flex')}>
							<Link to={'/le labo'} className={cx('link-brand', 'height')}>
								<img
									src={require('./image/Hang-nuoc-hoa-Maison-Matine.png')}
									alt={'nuoc hoa lelabo'}
									className={cx('brand-img')}
								/>
							</Link>

							<Link
								to={'/le labo'}
								className={cx('link-brand', 'flex-margin', 'height')}
							>
								<img
									src={require('./image/Nasomatto.png')}
									alt={'nuoc hoa lelabo'}
									className={cx('brand-img')}
								/>
							</Link>
						</div>
					</div>
				</div>
			</div>
			<hr className={cx('line-show')}></hr>
			{/* product-item */}
			<div className={cx('product-wrapper')}>
				<div className={cx('product-title')}>
					<h2 className={cx('product-title__h2')}>Sản Phẩm Nổi Bật</h2>
				</div>
				<div className={cx('product-item__wrapper')}>
					<ul className="uk-subnav" ref={ref}>
						<li>
							<Link className={cx('item-product')} to={''}>
								Nước hoa nam
							</Link>
						</li>
						<li>
							<Link className={cx('item-product')} to={''}>
								Nước hoa nữ
							</Link>
						</li>
						<li>
							<Link className={cx('item-product')} to={''}>
								Uniex
							</Link>
						</li>
					</ul>

					<ul className={cx('uk-switcher uk-margin')}>
						<li>
							<div className={cx('product__wrapper')}>
								<Carousel
									cols={5}
									rows={1}
									gap={8}
									loop
									showDots
									responsiveLayout={[
										{
											breakpoint: 800,
											cols: 1,
											rows: 1,
											gap: 8,
											loop: true,
											autoplay: 3000,
										},
									]}
								>
									{DB_FOR_MEN.map((data, index) => {
										return (
											<Carousel.Item key={index}>
												<div className={cx('product-item__chidren')}>
													<Link
														to={'/product' + data.path}
														className={cx('product-item__Link')}
													>
														<img
															src={data.image}
															alt={data.alt}
															className={cx('image-item')}
														/>
													</Link>

													<div className={cx('product-item__body')}>
														<Link
															to={'/product' + data.path}
															className={cx('product-item__title')}
														>
															<span> {data.brandName}</span>
														</Link>
														<Link
															to={'/product' + data.path}
															className={cx('product-item__content')}
														>
															<p className={cx('product-item__content')}>
																{data.title}
															</p>
														</Link>
													</div>
													<div className={cx('product-item__price')}>
														<span>{data.price.ml60}</span>
													</div>
												</div>
											</Carousel.Item>
										);
									})}
								</Carousel>
							</div>
						</li>
						<li>
							<div className={cx('product__wrapper')}>
								<Carousel
									cols={5}
									rows={1}
									gap={8}
									loop
									showDots
									responsiveLayout={[
										{
											breakpoint: 800,
											cols: 1,
											rows: 1,
											gap: 8,
											loop: true,
											autoplay: 3000,
										},
									]}
								>
									{DB_FOR_WOMEN.map((data, index) => {
										return (
											<Carousel.Item key={index}>
												<div className={cx('product-item__chidren')}>
													<Link
														to={'/product' + data.path}
														className={cx('product-item__Link')}
													>
														<img
															src={data.image}
															alt={data.alt}
															className={cx('image-item')}
														/>
													</Link>

													<div className={cx('product-item__body')}>
														<Link
															to={'/product' + data.path}
															className={cx('product-item__title')}
														>
															<span> {data.brandName}</span>
														</Link>
														<Link
															to={'/product' + data.path}
															className={cx('product-item__content')}
														>
															<p className={cx('product-item__content')}>
																{data.title}
															</p>
														</Link>
													</div>
													<div className={cx('product-item__price')}>
														<span>{data.price.ml60}</span>
													</div>
												</div>
											</Carousel.Item>
										);
									})}
								</Carousel>
							</div>
						</li>
						<li>
							<div className={cx('product__wrapper')}>
								<Carousel
									cols={5}
									rows={1}
									gap={8}
									loop
									showDots
									responsiveLayout={[
										{
											breakpoint: 800,
											cols: 1,
											rows: 1,
											gap: 8,
											loop: true,
											autoplay: 3000,
										},
									]}
								>
									{DB_FOR_UNISEX.map((data, index) => {
										return (
											<Carousel.Item key={index}>
												<div className={cx('product-item__chidren')}>
													<Link
														to={'/product' + data.path}
														className={cx('product-item__Link')}
													>
														<img
															src={data.image}
															alt={data.alt}
															className={cx('image-item')}
														/>
													</Link>

													<div className={cx('product-item__body')}>
														<Link
															to={'/product' + data.path}
															className={cx('product-item__title')}
														>
															<span> {data.brandName}</span>
														</Link>
														<Link
															to={'/product' + data.path}
															className={cx('product-item__content')}
														>
															<p className={cx('product-item__content')}>
																{data.title}
															</p>
														</Link>
													</div>
													<div className={cx('product-item__price')}>
														<span>{data.price.ml60}</span>
													</div>
												</div>
											</Carousel.Item>
										);
									})}
								</Carousel>
							</div>
						</li>
					</ul>
				</div>
			</div>
			<hr className={cx('line-show')}></hr>
			<div className={cx('container')}>
				<div className={cx('container__aa')}>
					<div className={cx('shop-title')}>
						<h2 className={cx('shop-title__h2')}>Tại sao chọn perfurm store</h2>
					</div>
					<div className={cx('shop-content')}>
						<div className={cx('shop-item')}>
							<FontAwesomeIcon icon={faShield} className={cx('shop-icon')} />
							<div className={cx('shop-item__title')}>sản phẩm chính hãng</div>
							<div className={cx('shop-item__content')}>
								<p>
									sản phẩm nước hoa được mua trực tiếp tại các store ở nước
									ngoài hoặc làm việc trực tiếp với các hãng, cam kết authentic
									100%
								</p>
							</div>
						</div>
						<div className={cx('shop-item')}>
							<FontAwesomeIcon icon={faTruck} className={cx('shop-icon')} />
							<div className={cx('shop-item__title')}>free ship toàn quốc</div>
							<div className={cx('shop-item__content')}>
								<p>
									áp dụng freeship cho tất cả các khách hàng trên toàn quốc.
									chúng tôi chưa áp dụng hình thức giao hàng quốc tế tại thời
									điểm này
								</p>
							</div>
						</div>
						<div className={cx('shop-item')}>
							<FontAwesomeIcon icon={faGift} className={cx('shop-icon')} />
							<div className={cx('shop-item__title')}>
								thành viên thân thiết
							</div>
							<div className={cx('shop-item__content')}>
								<p>
									thành viên vàng sẽ được giảm 5% / đơn hàng. với thành viên bạc
									khách được giảm 3% / đơn hàng.
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<hr className={cx('line-show')}></hr>
		</div>
	);
}

export default Home;
