import styles from './account.module.scss';
import classNames from 'classnames/bind';
import UIkit from 'uikit';
import { useEffect, useRef } from 'react';

const cx = classNames.bind(styles);

function Account() {
	const ref = useRef();
	useEffect(() => {
		UIkit.switcher(ref.current, {
			connect: '.uk-switcher',
			animation: 'uk-animation-fade',
		});
	}, []);

	return (
		<div className={cx('account-wrapper')}>
			<div className={cx('account-content')}>
				<div className={cx('account-content__wrapper')}>
					<div className={cx('account-content__body')}>
						<ul className="uk-subnav" ref={ref}>
							<li>
								<a href="#dangki" className={cx('account-fontsize')}>
									Đăng kí
								</a>
							</li>
							<li>
								<a href="#dangnhap" className={cx('account-fontsize')}>
									Đăng nhập
								</a>
							</li>
						</ul>

						<ul className="uk-switcher uk-margin">
							<li>
								<div className={cx('account-signup')}>
									<div className={cx('signup-container')}>
										<div className={cx('row')}>
											<label>Họ và tên</label>
										</div>
										<div className={cx('row')}>
											<input type={'text'} placeholder={'họ và tên'} />
										</div>
										<div className={cx('row')}>
											<label>Email</label>
										</div>
										<div className={cx('row')}>
											<input type={'text'} placeholder={'Email'} />
										</div>
										<div className={cx('row')}>
											<label>Password</label>
										</div>
										<div className={cx('row')}>
											<input type={'password'} placeholder={'Password'} />
										</div>
										<div className={cx('row')}>
											<label>Confirm Password</label>
										</div>
										<div className={cx('row')}>
											<input
												type={'password'}
												placeholder={'Confirm Password'}
											/>
										</div>
										<div className={cx('row')}>
											<div className={cx('btn-signup__wrapper')}>
												<button className={cx('btn-signup')}>Đăng kí</button>
											</div>
										</div>
									</div>
								</div>
							</li>
							<div className={cx('account-signup')}>
								<div className={cx('signup-container')}>
									<div className={cx('row')}>
										<label>Email</label>
									</div>
									<div className={cx('row')}>
										<input type={'text'} placeholder={'Email'} />
									</div>
									<div className={cx('row')}>
										<label>Password</label>
									</div>
									<div className={cx('row')}>
										<input type={'password'} placeholder={'Password'} />
									</div>
									<div className={cx('row', 'checkbox')}>
										<input
											type={'checkbox'}
											placeholder={'Password'}
											className={cx('checkbox-login')}
										/>
										<span>ghi nhớ mật khẩu</span>
									</div>

									<div className={cx('row')}>
										<div className={cx('btn-signup__wrapper')}>
											<button className={cx('btn-signup')}>Đăng nhập</button>
										</div>
									</div>

									<div className={cx('row')}>
										<a href="/quenmatkhau" className={cx('forgot-password')}>
											Quên mật khẩu
										</a>
									</div>
								</div>
							</div>
						</ul>
					</div>
				</div>
			</div>
		</div>
	);
}

export default Account;
