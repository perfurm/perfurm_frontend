import './product.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from './product.module.scss';
import classNames from 'classnames/bind';
import {
	faVenusMars,
	faPlus,
	faMinus,
} from '@fortawesome/free-solid-svg-icons';
import { useRef, useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import UIkit from 'uikit';
import Carousel from 'better-react-carousel';
import { Store, action } from '../../store';

const cx = classNames.bind(styles);

function Product({ data = {} }) {
	const [dungtich, setDungtich] = useState('0');
	const [quantity, setQuantity] = useState(1);
	const [priceSel, setPriceSel] = useState('');

	useEffect(() => {
		if (dungtich === '60') {
			setPriceSel(data.price.ml60);
		} else if (dungtich === '100') {
			setPriceSel(data.price.ml100);
		} else {
			setPriceSel(data.price.ml60 || data.price.ml100);
		}
	}, [dungtich]);

	let refInput = useRef();

	function handleMinus() {
		if (refInput.current.value <= 1) {
			setQuantity(1);
		} else {
			setQuantity(quantity - 1);
		}
	}

	const ref = useRef();
	useEffect(() => {
		UIkit.switcher(ref.current, {
			connect: '.uk-switcher',
			animation: 'uk-animation-fade',
		});
	}, []);

	const [DB, setDB] = useState([]);
	useEffect(() => {
		fetch('https://perfurm-backend.onrender.com/product/productdb', {})
			.then((res) => res.json())
			.then((data) => setDB(data));
	}, []);

	const [state, dispatch] = Store();

	return (
		<div className={cx('product-wrapper')}>
			<div className={cx('container')}>
				<div className={cx('product-content')}>
					<div className={cx('content-left')}>
						<div className={cx('content-image')}>
							<img
								src={data.image}
								alt="product"
								className={cx('image-product')}
							></img>
						</div>
						<div className={cx('description')}>
							<h3 className={cx('description-title')}>Mô tả</h3>
							<p className={cx('description-content')}>{data.description}</p>
						</div>
					</div>
					<div className={cx('content-right')}>
						<div className={cx('summary')}>
							<p className={cx('brand-name')}>{data.brandName}</p>
							<h1 className={cx('prouct-title__entire')}>{data.title}</h1>
							<p className={cx('gender')}>
								<FontAwesomeIcon
									icon={faVenusMars}
									className={cx('gender-icon')}
								/>
								<span className={cx('gender-name')}>{data.gender}</span>
							</p>
							<p className={cx('price')}>{priceSel}đ</p>
							<div className={cx('dungtich-wrapper')}>
								<span className={cx('dungtich-title')}>Dung tích</span>
								<select
									className={cx('dungtich-sel')}
									value={dungtich}
									onChange={(e) => {
										setDungtich(e.target.value);
									}}
								>
									<option value={'0'}>Chon dung tich</option>
									<option value={'60'} hidden={data.price.ml60 === undefined}>
										60ml
									</option>
									<option value={'100'} hidden={data.price.ml100 === undefined}>
										100ml
									</option>
								</select>
							</div>
							<div className={cx('add-to__cart__wrapper')}>
								<div className={cx('quantity__wrapper')}>
									<span
										className={cx('quantity-title__plus')}
										onClick={() => {
											if (quantity === '') {
												setQuantity(1);
											} else {
												setQuantity((quantity) => {
													return parseInt(quantity) + 1;
												});
											}
										}}
									>
										<FontAwesomeIcon icon={faPlus} />
									</span>
									<input
										className={cx('quantity-value')}
										value={quantity}
										ref={refInput}
										onChange={(e) => {
											if (isNaN(e.target.value)) {
												setQuantity('');
											} else {
												setQuantity(e.target.value);
											}
										}}
									/>
									<span
										className={cx('quantity-title__minus')}
										onClick={handleMinus}
									>
										<FontAwesomeIcon icon={faMinus} />
									</span>
								</div>
								<div className={cx('button-cart__wrapper')}>
									<button
										className={cx('button-cart')}
										onClick={() => {
											dispatch(
												action.addProduct({
													...data,
													soluong: quantity,
													priceSelected: priceSel,
												})
											);
										}}
									>
										THÊM VÀO GIỎ HÀNG
									</button>
								</div>
							</div>
							<div className={cx('product-detail__top')}>
								<ul className="uk-subnav align-left" ref={ref}>
									<li className={cx('item-product')}>
										<Link to={''}>hương</Link>
									</li>
									<li className={cx('item-product')}>
										<Link to={''}>đặc điểm</Link>
									</li>
									<li className={cx('item-product')}>
										<Link to={''}>Khuyên dùng</Link>
									</li>
								</ul>

								<ul className={cx('uk-switcher uk-margin')}>
									<li>
										<div className={cx('frangment')}>
											<ul className={cx('frangment-list')}>
												<li className={cx('frangment-list__li')}>
													<span>tone hương </span>
													<ul className={cx('ul-style')}>
														<li>hổ phách,</li>
														<li>hương gỗ,</li>
													</ul>
												</li>
												<li className={cx('frangment-list__li')}>
													<span>hương đầu</span>
													<ul className={cx('ul-style')}>
														<li>hạnh nhân trắng,</li>
														<li>hoa nghệ tây</li>
													</ul>
												</li>
												<li className={cx('frangment-list__li')}>
													<span>hương giữa</span>
													<ul className={cx('ul-style')}>
														<li>gỗ tuyết tùng</li>
														<li>hoa nhài</li>
													</ul>
												</li>
												<li className={cx('frangment-list__li')}>
													<span>hương cuối</span>
													<ul className={cx('ul-style')}>
														<li>long diên hương</li>
														<li>hoa nghệ tây</li>
													</ul>
												</li>
											</ul>
										</div>
									</li>
									<li>
										<div className={cx('feature-wrapper')}>
											<ul className={cx('feature')}>
												<li>
													<span>phát hành</span>
													2017
												</li>
												<li>
													<span>giới tính</span>
													men
												</li>
												<li>
													<span>độ tuổi</span>
													22
												</li>
												<li>
													<span>độ lưu mùi</span>
													<div className={cx('fragant_saving')}>
														<div className={cx('')}>
															<span>3</span>
														</div>
														<div className={cx('')}>
															<span>6</span>
														</div>
														<div className={cx('active')}>
															<span>8</span>
														</div>
														<div className={cx('')}></div>
													</div>
												</li>
											</ul>
										</div>
									</li>
									<li>
										<div className={cx('season-wrapper')}>
											<ul className={cx('seasion')}>
												<li>
													<span>mùa</span>
													<ul>
														<li>
															<span>xuân</span>
															<div className={cx('range-silce')}>
																<span style={{ width: '60%' }}></span>
															</div>
														</li>
														<li>
															<span>hạ</span>
															<div className={cx('range-silce')}>
																<span style={{ width: '40%' }}></span>
															</div>
														</li>
														<li>
															<span>thu</span>
															<div className={cx('range-silce')}>
																<span style={{ width: '30%' }}></span>
															</div>
														</li>
														<li>
															<span>đông</span>
															<div className={cx('range-silce')}>
																<span style={{ width: '80%' }}></span>
															</div>
														</li>
													</ul>
												</li>
												<li>
													<span>thời gian</span>
													<ul>
														<li>
															<span>ngày</span>
															<div className={cx('range-slice')}>
																<span style={{ width: '40%' }}></span>
															</div>
														</li>
														<li>
															<span>đêm</span>
															<div className={cx('range-slice')}>
																<span style={{ width: '70%' }}></span>
															</div>
														</li>
													</ul>
												</li>
											</ul>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<hr></hr>
				<div className={cx('product-relative')}>
					<div className={cx('product-relative-title')}>sản phẩm liên quan</div>
					<div className={cx('product-relative__content')}>
						<div className={cx('product-relative__content-item')}>
							<Carousel
								cols={5}
								rows={1}
								gap={8}
								loop
								showDots
								responsiveLayout={[
									{
										breakpoint: 800,
										cols: 1,
										rows: 1,
										gap: 8,
										loop: true,
										autoplay: 3000,
									},
								]}
							>
								{DB.map((db, index) => {
									if (data.gender === db.gender) {
										return (
											<Carousel.Item key={index}>
												<div className={cx('product-item__chidren')}>
													<a
														href={'/product' + db.path}
														className={cx('product-item__Link')}
													>
														<img
															src={db.image}
															alt={db.alt}
															className={cx('image-item')}
														/>
													</a>

													<div className={cx('product-item__body')}>
														<a
															href={'/product' + db.path}
															className={cx('product-item__title')}
														>
															<span> {db.brandName}</span>
														</a>
														<a
															href={'/product' + db.path}
															className={cx('product-item__content')}
														>
															<p className={cx('product-item__content')}>
																{db.title}
															</p>
														</a>
													</div>
													<div className={cx('product-item__price')}>
														<span>{db.price.ml60 || db.price.ml100}</span>
													</div>
												</div>
											</Carousel.Item>
										);
									}
								})}
							</Carousel>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
}

export default Product;
